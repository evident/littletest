package de.tinytall.studios.tests.littletest;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class LittleTest extends ApplicationAdapter {
	public PerspectiveCamera cam;
	public CameraInputController inputController;
	public ModelBatch modelBatch;
	public Model model;
	public ModelInstance instance;
	public Environment environment;
	private FrameBuffer depthFB;

	private ShaderProgram ssaoShaderProgram;

	int u_depthMap;

	private int u_projection_matrix;
	private int u_view_matrix;
	private int u_projectionview_matrix;
	private int u_inv_projectionview_matrix;
	private int u_inv_projection_matrix;

	private int u_farDistance;
	private int u_nearDistance;
	private Mesh fullScreenQuad;
	private Matrix4 tmpInvProjection = new Matrix4();


	@Override
	public void create () {
		modelBatch = new ModelBatch(new DepthShaderProvider());

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, .4f, .4f, .4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

		cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(10f, 10f, 10f);
		cam.lookAt(0, 0, 0);
		cam.near = 0.1f;
		cam.far = 300f;
		cam.update();

		ModelBuilder modelBuilder = new ModelBuilder();
		model = modelBuilder.createBox(5f, 5f, 5f, new Material(ColorAttribute.createDiffuse(Color.GREEN)), VertexAttributes.Usage.Position
				| VertexAttributes.Usage.Normal);
		instance = new ModelInstance(model);

		this.inputController = new CameraInputController(cam);
		Gdx.input.setInputProcessor(this.inputController);


		this.depthFB = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);


		String ssaoVert = Gdx.files.internal("data/frotty_ssao_vert.glsl").readString();
		String ssaoFrag = Gdx.files.internal("data/frotty_ssao_frag.glsl").readString();
		ssaoShaderProgram = new ShaderProgram(ssaoVert, ssaoFrag);
		if (!ssaoShaderProgram.isCompiled())
			throw new GdxRuntimeException(ssaoShaderProgram.getLog());

		u_depthMap = ssaoShaderProgram.getUniformLocation("u_depthMap");

		u_projection_matrix = ssaoShaderProgram.getUniformLocation("u_projection_matrix");
		u_inv_projection_matrix = ssaoShaderProgram.getUniformLocation("u_inv_projection_matrix");
		u_view_matrix = ssaoShaderProgram.getUniformLocation("u_view_matrix");
		u_projectionview_matrix = ssaoShaderProgram.getUniformLocation("u_projectionview_matrix");
		u_inv_projectionview_matrix = ssaoShaderProgram.getUniformLocation("u_inv_projectionview_matrix");

		u_farDistance = ssaoShaderProgram.getUniformLocation("u_farDistance");
		u_nearDistance = ssaoShaderProgram.getUniformLocation("u_nearDistance");

		fullScreenQuad = MeshUtils.createFullScreenQuad();


	}

	@Override
	public void render () {
		inputController.update();

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());


		depthFB.begin();

		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		modelBatch.begin(cam);
		modelBatch.render(instance, environment);
		modelBatch.end();
		depthFB.end();


		depthFB.getColorBufferTexture().bind(1);

		ssaoShaderProgram.begin();
		ssaoShaderProgram.setUniformi(u_depthMap, 1);


		ssaoShaderProgram.setUniformMatrix(u_projection_matrix, cam.projection);

		tmpInvProjection.set(cam.projection).inv();

		ssaoShaderProgram.setUniformMatrix(u_inv_projection_matrix, tmpInvProjection);
		ssaoShaderProgram.setUniformMatrix(u_view_matrix, cam.view);
		ssaoShaderProgram.setUniformMatrix(u_projectionview_matrix, cam.combined);
		ssaoShaderProgram.setUniformMatrix(u_inv_projectionview_matrix, cam.invProjectionView);

		ssaoShaderProgram.setUniformf(u_farDistance, cam.far);
		ssaoShaderProgram.setUniformf(u_nearDistance, cam.near);


		fullScreenQuad.render(ssaoShaderProgram, GL20.GL_TRIANGLE_FAN);

		ssaoShaderProgram.end();

		depthFB.getColorBufferTexture().bind(0);

	}

	@Override
	public void dispose () {
		modelBatch.dispose();
		model.dispose();
	}


	public void resume () {
	}

	public void resize (int width, int height) {
	}

	public void pause () {
	}
}
