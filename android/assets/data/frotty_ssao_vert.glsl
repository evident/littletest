attribute vec3 a_position;
attribute vec2 a_texCoord0;

uniform mat4 u_worldTrans;
uniform mat4 u_projTrans;

varying vec2 uv;

void main() {

    gl_Position = vec4(a_position, 1.0);

    uv = a_texCoord0;

}