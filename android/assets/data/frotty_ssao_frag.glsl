uniform sampler2D u_depthMap;


uniform mat4 u_projection_matrix;
uniform mat4 u_inv_projection_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projectionview_matrix;
uniform mat4 u_inv_projectionview_matrix;

uniform float u_nearDistance;
uniform float u_farDistance;

varying vec2 uv;



float getShadowness(vec2 offset)
{
    const vec4 bitShifts = vec4(1.0, 1.0 / 255.0, 1.0 / 65025.0, 1.0 / 160581375.0);
    return dot(texture2D(u_depthMap, uv + offset).rgba, bitShifts);
}
float ld(float depth) {
        float near = u_nearDistance;
        float far = u_farDistance;
   return (2.0 * near) / (far + near - depth * (far - near));
}


void main(void)
{


    //gl_FragColor = texture2D(u_depthMap, uv).rgba;

    gl_FragColor = vec4(vec3(ld(getShadowness(vec2(0.0,0.0)))), 1.0);


}